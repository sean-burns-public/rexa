function renderRequirementsTree() {

  /* Pre-load the diagramScript string with the diagram's settings. */
  var diagramScript = 'digraph Requirements { \
    layout=dot; \
    splines=true; \
    ratio=auto; \
    color=black; \
    bgcolor=white; \
    overlap=false; \
    rankdir=LR; \
    ';

  /* Merge-in the requirements that have been generated in a string within a separate JS file. */
  diagramScript = diagramScript + requirements;

  /* Terminate the 'digraph Requirements' object. */
  diagramScript = diagramScript + '}';

  var viz = new Viz();
  viz.renderSVGElement(diagramScript)
    .then(function(element) {
      document.getElementById("requirements_tree").appendChild(element);
      var panZoomTiger = svgPanZoom('#requirements_tree svg', {
        panEnabled: true
, controlIconsEnabled: true
, zoomEnabled: true
, dblClickZoomEnabled: true
, mouseWheelZoomEnabled: true
, preventMouseEventsDefault: true
, zoomScaleSensitivity: 0.2
, minZoom: 0.5
, maxZoom: 10
, fit: true
, contain: false
, center: true
, refreshRate: 'auto'
, beforeZoom: function(){}
, onZoom: function(){}
, beforePan: function(){}
, onPan: function(){}
, onUpdatedCTM: function(){}
, eventsListenerElement: null
});


    })

      /* When rendering graphs with user input, or any other graph that may
        include syntax errors, you will probably need to recreate the Viz
        instance after catching syntax errors thrown by the render functions.
        Syntax errors can sometimes cause the Graphviz DOT parsing code to
        enter an error state it can't recover from, meaning that all
        subsequent input will throw errors. */
      .catch(error => {

        /* Create a new Viz instance. */
        viz = new Viz();

        /* Display the error. */
        console.error(error);
      });
}
